let error = "";

let name = document.getElementById("name");
let email = document.getElementById("mail");
let phone = document.getElementById("phone");
const element = document.querySelector("form");

element.addEventListener("submit", event => {
  event.preventDefault();
  validateForm();
});

function validateForm() {
  if (
    validateName(name) == false ||
    validateEmail(email) == false ||
    validatePhone(phone) == false
  ) {
    return false;
  } else {
    return true;
  }
}

function validateName(name) {
  if (name.value < 2) {
    error = "Du må ha et ordentlig navn.";
    name.focus();
    document.getElementById("errorMessage").innerHTML = error;
    return false;
  } else {
    return true;
  }
}

/*Sjekker om mail-feltet er tomt og om @ er midt i e-postadressen, med
påfølgende punktum, og domene.*/

function validateEmail(email) {
  if (email.value == "" || email.value.indexOf("@") == -1) {
    error = "Du må skrive en fullverdig e-post adresse";
    mail.focus();
    document.getElementById("errorMessage").innerHTML = error;
    return false;
  } else if (email.value.indexOf("@") != 0) {
    if (
      !(
        email.value.indexOf("@") + 1 < email.value.lastIndexOf(".") &&
        email.value.indexOf(".") < email.value.length - 2
      )
    ) {
      return false;
    } else {
      return true;
    }
  }
}

function validatePhone(phone) {
  if (phone.value < 8) {
    error = "Telefonnummeret må ha 8 siffer.";
    phone.focus();
    document.getElementById("errorMessage").innerHTML = error;
    return false;
  }
  return true;
}
